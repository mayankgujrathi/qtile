#!/usr/bin/env python
# require python(3.6+)
from subprocess import run
import os, json, sys

LOG_FILE = '/tmp/notification_log.json'
LOG_FILE_INDENT = 1
ICONS_DIR = '/usr/share/icons/Papirus/48x48/'
DEFAULT_ICON_PATH = ICONS_DIR + 'apps/bell.svg'
SEP = "\x0f"
CONFIG_PATH = '~/.config/qtile/.configs/rofi/configNotif.rasi'
MSG_TITLE_LEN = 22
NO_NOTIFY_TITLE = 'No Notification'
DEFAULT_MSG_TITLE = 'no title'
DEFAULT_MSG_SUMY = 'no summary'
DEFAULT_MSG_APP_NAME = 'unknown app'

def _gen_notify(appname: str, time: str, title: str, sumy: str, icon_path: str):
    app_name_len = MSG_TITLE_LEN - time.__len__()
    app_name = appname if appname.__len__() < app_name_len else appname[0:app_name_len] + '...'
    spaces = ' ' * (MSG_TITLE_LEN - app_name.__len__())
    return rf'''<b>{app_name}{spaces}{time}</b>
{title}
{sumy} \0icon\x1f{icon_path}'''

def _gen_path(item: dict):
    def _gen_call_with(name: str):
        return run(f'fd {name} {ICONS_DIR} | head -1', shell=True, capture_output=True, text=True).stdout.strip()

    if os.path.exists(item.get('d-type')):
        return item.get('d-type')
    if item.get('appname'):
        if 'whatsapp' in item.get('appname').lower():
            return _gen_call_with('whatsapp')
        res = _gen_call_with(item.get('appname'))
        if res.strip(): return res
    elif item.get('d-type'):
        return _gen_call_with(item.get('d-type')) or DEFAULT_ICON_PATH
    return DEFAULT_ICON_PATH

def _remove(time: str, title: str, sumy: str):
    stored: list = json.loads(run(f'cat {LOG_FILE}', shell=True, capture_output=True).stdout)
    print(time, title, sumy)
    x = filter(lambda item: not all([
        item.get('title').strip() == title.strip(), 
        item.get('sumy').strip() == sumy.strip(), 
        item.get('tsp').strip() == time.strip()
    ]), stored)
    with open(LOG_FILE, 'w') as ptr:
        json.dump(
            list(x),
            ptr,
            ensure_ascii = False,
            indent=LOG_FILE_INDENT,
        )

def clean(data:str) -> str:
    return data.replace('\n', ';').replace('\\', '') \
    .replace('/', '').replace('<', '').replace('>', '') \
    .replace('\"', '').replace('\'', '') \
    .replace('=', '').replace('$', '') 

def main():
    def ret_fail(body=''):
        run(f'notify-send "{NO_NOTIFY_TITLE}" "{body}"', shell=True)
        sys.exit(1)

    if not os.path.exists(LOG_FILE):
        ret_fail()

    try:
        storage = json.loads(run(f'cat {LOG_FILE}', shell=True, capture_output=True).stdout)
    except Exception as e:
        ret_fail(str(e))
    if len(storage) <= 0:
        ret_fail()

    rofi_input = SEP.join([_gen_notify(
        time=':'.join(item.get('tsp').split(' ')),
        title=clean(item.get('title')) or DEFAULT_MSG_TITLE,
        sumy=clean(item.get('sumy')) or DEFAULT_MSG_SUMY,
        appname=clean(item.get('appname')) or DEFAULT_MSG_APP_NAME,
        icon_path=_gen_path(item).replace('\n', ';'),
        ) for item in storage][::-1])

    ROFI_CALL = f'echo -ne "{rofi_input}" | rofi -markup-rows -theme {CONFIG_PATH} -dmenu -eh 3 -sep \'{SEP}\' -p "Notification Center ({len(storage)})" -no-fixed-num-lines -lines 12 -i -no-config'
    selected = run(ROFI_CALL, shell=True, capture_output=True, text=True).stdout.splitlines()
    # print(selected)
    selected.__len__() > 0 and _remove(f"{selected[0][-12]+selected[0][-11]} {selected[0][-9]+selected[0][-8]} {selected[0][-6]+selected[0][-5]}", selected[1], selected[2])

if __name__ == '__main__':
    main()

#!/usr/bin/env python
#require python(3.6+)

"""
example item will be
"label \0icon\x1f/usr/share/icons/Papirus/48x48/apps/bell.svg\x0f{{nextone}}"
"""
from enum import Enum
from subprocess import run

ROFI_CONFIG = '~/.config/qtile/.configs/rofi/dialog.rasi'
SEP = '\x0f'

# CMD for actions
CMD_POWER_OFF = '/usr/bin/poweroff'
CMD_REBOOT = '/usr/bin/reboot'
CMD_LOGOUT = "qtile shell -c 'shutdown()'"
CMD_LOCK = 'i3lock-fancy'

# Yes or no prompt
YES = 'Yes'
NO = 'No'
YES_ICON = '/usr/share/icons/Papirus/48x48/emblems/vcs-normal.svg'
NO_ICON = '/usr/share/icons/Papirus/48x48/emblems/vcs-conflicting.svg'

# for leave calls
POWER_OFF = 'Poweroff'
REBOOT = 'Reboot'
LOGOUT = 'Logout'
LOCK = 'Lock'
POWER_OFF_ICON = '/usr/share/icons/Papirus/48x48/apps/system-shutdown.svg'
REBOOT_ICON = '/usr/share/icons/Papirus/48x48/apps/system-reboot.svg'
LOGOUT_ICON = '/usr/share/icons/Papirus/48x48/apps/system-log-out.svg'
LOCK_ICON = '/usr/share/icons/Papirus/48x48/apps/system-lock-screen.svg'


def _gen_template(args) -> str:
    return SEP.join([f'{args[i]} \0icon\x1f{args[i+1]}' for i in range(0, len(args) -1, 2)])

def do_yes_or_no_call():
    template = [YES, YES_ICON, NO, NO_ICON]
    rofi_call = f'echo -ne {_gen_template(template).__repr__()} | rofi -markup-rows -theme {ROFI_CONFIG} -dmenu -eh 3 -sep \'{SEP}\' -lines 1 -i -no-config'
    return run(rofi_call, shell=True, capture_output=True, text=True).stdout.strip()

def do_go_off_call():
    template = [LOCK, LOCK_ICON, LOGOUT, LOGOUT_ICON, REBOOT, REBOOT_ICON, POWER_OFF, POWER_OFF_ICON]
    rofi_call = f'echo -ne {_gen_template(template).__repr__()} | rofi -markup-rows -theme {ROFI_CONFIG} -dmenu -eh 3 -sep \'{SEP}\' -lines 1 -i -no-config'
    return run(rofi_call, shell=True, capture_output=True, text=True).stdout.strip()

def main():
    selected = do_go_off_call()
    if not selected.strip():
        return
    if selected == LOCK and do_yes_or_no_call().strip() == YES:
        run(CMD_LOCK, shell=True)
    elif selected == LOGOUT and do_yes_or_no_call().strip() == YES:
        run(CMD_LOGOUT, shell=True)
    elif selected == REBOOT and do_yes_or_no_call().strip() == YES:
        run(CMD_REBOOT, shell=True)
    elif selected == POWER_OFF and do_yes_or_no_call().strip() == YES:
        run(CMD_POWER_OFF, shell=True)

if __name__ == '__main__':
    main()

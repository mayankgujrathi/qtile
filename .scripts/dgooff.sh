#!/usr/bin/env bash

mpoweroff="poweroff"
mreboot="reboot"
mlogout="logout"
mlock="lock"

function yaorna() {
	res=$(echo -ne "yes""\n""no" \
	| dmenu  -nb "#222" -nf "#fff" -sb "#555" -sf "#fff" -nhb "#777" -nhf "#000" -shb "#aaa"  -shf "#222" -i -p 'Continue: ')
	case $res in
		yes)
			echo 0
			break
		;;
		* )
			echo 1
			break
		;;
	esac
}

selected=$(echo -ne $mlock"\n"$mlogout"\n"$mreboot"\n"$mpoweroff \
	| dmenu -nb "#222" -nf "#fff" -sb "#555" -sf "#fff" -nhb "#777" -nhf "#000" -shb "#aaa"  -shf "#222"  -i -p 'Action: ')

[ -z $selected ] && exit 1

case $selected in
	logout)
		if [ `yaorna` -eq 0 ]; then 
			qtile shell -c 'shutdown()'
		fi
		break
	;;
	lock)
		if [ `yaorna` -eq 0 ]; then 
			# require i3lock-fancy
			i3lock-fancy
		fi
		break
	;;
	reboot)
		if [ `yaorna` -eq 0 ]; then 
			/usr/bin/reboot
		fi
		break
	;;
	poweroff)
		if [ `yaorna` -eq 0 ]; then 
			/usr/bin/poweroff
		fi
		break
	;;
	* )
		exit 1
		break
	;;
esac

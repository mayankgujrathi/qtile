#!/usr/bin/env python
# require python(3.6+)

from rofi_notif_center import NO_NOTIFY_TITLE, DEFAULT_MSG_APP_NAME, DEFAULT_MSG_SUMY, DEFAULT_MSG_TITLE, clean

LOG_FILE = '/tmp/notification_log.json'
LOG_FILE_INDENT = 4

from datetime import datetime as dt
from sys import argv
from subprocess import run
import os, json

def _log(*args):
    with open("/home/mayank/stdpyout", "a") as out:
        out.writelines(f'\n[{dt.now().__str__()}]')
        for o in args: out.writelines('\n'+str(o))

def pre_check():
    def _gen_template():
        with open(LOG_FILE, 'w') as out:
            out.write('[]')
    if not os.path.exists(LOG_FILE):
        _gen_template()
        return
    value = ''
    with open(LOG_FILE, 'r') as out:
        value = out.read()
    #_log(value)
    if not value:
        _gen_template()

def main(args: list):
    if args[0] == 'notify-send' and args[1] == NO_NOTIFY_TITLE:
        return
    pre_check()
    stored = json.loads(run(f'cat {LOG_FILE}', shell=True, capture_output=True).stdout)
    with open(LOG_FILE, 'w') as ptr:
        tsp = dt.now().strftime('%H %M %S')
        json.dump(
            [*stored, {
                'tsp': tsp,
                'appname': clean(args[0]).replace('\\n', ';') or DEFAULT_MSG_APP_NAME,
                'title': clean(args[1]).replace('\\n', ';') or DEFAULT_MSG_TITLE,
                'sumy': clean(args[2]).replace('\\n', ';') or DEFAULT_MSG_SUMY,
                'd-type': args[3],
                'l-type': args[4]
            }],
            ptr,
            ensure_ascii = False,
            indent=LOG_FILE_INDENT,
        )

if __name__ == '__main__':
    # _log(argv[1:])
    main(argv[1:])

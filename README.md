# Qtile Window Manager Configuration (myk-qtile)
This is my configuration for qtile window manager


![DemoGIF](/uidemo.gif)
![ScreenShot](/screenshot.png)

## Installation for Arch Based

### Step: 1 Installing Dependencies (using yay, you can use any AUR  helper)

```
yay -S qtile rofi picom-jonaburg-git i3lock-fancy xclip ttf-roboto polkit-gnome flameshot pnmixer network-manager-applet xfce4-power-manager qt5-styleplugins ttf-ubuntu-font-family nerd-fonts-ubuntu-mono nerd-fonts-jetbrains-mono papirus-icon-theme dmenu-distrotube-git xwallpaper alacritty materia-gtk-theme lxappearance dunst capitaine-cursors fd qt5ct
```

### extra (common apps I use)

```
yay -S thunar thunar-archive-plugin archiver mpv celluloid freetube-bin vscodium-bin-marketplace zathura zathura-pdf-poppler sxiv exa lf-bin htop aria2-git lightdm lightdm-webkit2-greeter obs-studio baobab droidcam onlyoffice-bin stacer cronie
```

### extra (for gaming)

for this to work enable *multilib* in pacman configuration

```
sudo pacman -S wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader wine lutris
```

### Step: 2 Cloning configuration

```
git clone https://github.com/mayankgujrathi/myk-qtile ~/.config/qtile
```

### Step: 3 Set the themes

Start `lxappearance` to activate the **icon** theme and **GTK** theme.
Note: for cursor theme, edit `~/.icons/default/index.theme` and `~/.config/gtk3-0/settings.ini`, for the change to also show up in applications run as root, copy the 2 files over to their respective place in `/root`.


### Step: 4 Same theme for Qt/KDE applications and GTK applications, and fix missing indicators

First install `qt5-style-plugins` (debian) | `qt5-styleplugins` (arch) and add this to the bottom of your `/etc/environment`

```bash
XDG_CURRENT_DESKTOP=Unity
QT_QPA_PLATFORMTHEME=gtk2 or qt5ct
```

The first variable fixes most indicators (especially electron based ones!), the second tells Qt and KDE applications to use your gtk2 theme set through lxappearance.

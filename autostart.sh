#!/bin/sh 

dunst -conf $HOME/.config/qtile/.configs/dunst/dunstrc &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)
picom --experimental-backends --config $HOME/.config/qtile/.configs/picom/picom.conf &
# require Xorg-server
xwallpaper --zoom $HOME/.config/qtile/wall/wall.png &
volumeicon &
nm-applet &
numlockx on &

# apps
